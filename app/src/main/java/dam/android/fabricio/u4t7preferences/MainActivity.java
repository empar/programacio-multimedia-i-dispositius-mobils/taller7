package dam.android.fabricio.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private Spinner spinnercolor;

    private EditText etScore;
    private Button btQuit;
    private RadioButton Difficulty;
    public int indexButton;
    private RadioGroup rgDifficulty;
    private CheckBox SoundActive;

    View targetView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        targetView = (View) findViewById(R.id.mainlayaout);
        etPlayerName = findViewById(R.id.etPlayerName);
        spinnerLevel = findViewById(R.id.spinnerLevel);
        rgDifficulty = (RadioGroup) findViewById(R.id.rgDifficulty);
        spinnercolor = findViewById(R.id.spinnerColor);
        SoundActive = findViewById(R.id.cbSound);
        indexButton=0;

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.levels, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);

        ArrayAdapter<CharSequence> spinnerColorAdapter = ArrayAdapter.createFromResource(this, R.array.nameColor, android.R.layout.simple_spinner_item);
        spinnerColorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnercolor.setAdapter(spinnerColorAdapter);

        spinnercolor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Resources res = getResources();
                String[] colors = res.getStringArray(R.array.valueColor);
                targetView.setBackgroundColor(Color.parseColor(colors[position]));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });

        etScore = findViewById(R.id.etScore);

        btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        rgDifficulty.setOnCheckedChangeListener(this);
        //rgDifficulty.setId(Difficulty.getId());


    }


    @Override
    protected void onPause() {
        String clave="CUIDADO";

        Log.isLoggable(clave, indexButton);
        super.onPause();
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putInt("levelColor", spinnercolor.getSelectedItemPosition());
        editor.putInt("Diffuculty", indexButton);
        editor.putBoolean("SoundActive", SoundActive.isChecked());

        editor.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        etPlayerName.setText(myPreferences.getString("PlayerName", "unknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        spinnercolor.setSelection(myPreferences.getInt("levelColor", 0));
        Difficulty = (RadioButton) rgDifficulty.getChildAt(myPreferences.getInt("Diffuculty", 0));
        rgDifficulty.check(Difficulty.getId());
        SoundActive.setChecked(myPreferences.getBoolean("SoundActive", false));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rbEasy:
               indexButton=0;
                break;
            case R.id.rbNormal:
                indexButton=1;
            case R.id.rbHard:
                indexButton=2;
                break;
            case R.id.rbVeryHard:
                indexButton=3;
                break;
            case R.id.rbExpert:
                indexButton=4;
                break;
        }
    }

}